package malves.blog.jenkins;

/**
 * @author malves
 *
 */
public class Imposto {

	/**
	 * 
	 */
	public Imposto() {
	}

	/**
	 * @param receita
	 * @return
	 */
	public Double calcula(Receita receita) {
		return receita.getValor() * 0.1;
	}

}