package malves.blog.jenkins;


/**
 * @author malves
 *
 */
public class Receita {

	private double valor;

	/**
	 * @param valor
	 */
	public Receita(double valor) {
		this.valor = valor;
	}

	/**
	 * @return
	 */
	public double getValor() {
		return valor;
	}

}
