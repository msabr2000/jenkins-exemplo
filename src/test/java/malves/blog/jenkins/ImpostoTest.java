package malves.blog.jenkins;
import static org.junit.Assert.assertEquals;
import malves.blog.jenkins.Imposto;
import malves.blog.jenkins.Receita;

import org.junit.Test;

/**
 * @author malves
 *
 */
public class ImpostoTest {
	
	// Valor esperado
	static Double valorEsperado = new Double(10.0);

	@Test
	public void calculaImpostoDeDezPorCento() {
		
		Receita receita = new Receita(100.0);
		Imposto imposto = new Imposto();
		
		Double valorCalculado = imposto.calcula(receita);

		assertEquals("Valor esperado é diferente do valor calculado!", valorEsperado, valorCalculado , Double.MIN_VALUE);
	}
}
